output "ecr_repository_arn" {
  value = aws_ecr_repository.my_repository[0].arn  # Accessing the ARN of the first instance
}
