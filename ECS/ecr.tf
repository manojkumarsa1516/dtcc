provider "aws" {
  region = "eu-west-3"
  # Add other AWS provider configurations as needed
}

# Input variables
variable "create_ecr_repository" {
  description = "Set to true if you want to create the ECR repository, false otherwise"
  type        = bool
  default     = true
}

variable "attach_ecr_policy" {
  description = "Set to true if you want to attach the IAM policy for CodeBuild to the ECR repository, false otherwise"
  type        = bool
  default     = true
}

# Create the ECR repository if it doesn't exist
resource "aws_ecr_repository" "my_repository" {
  count = var.create_ecr_repository ? 1 : 0  # Create only if specified
  name  = "dtcc-myrepo"
  image_tag_mutability = "MUTABLE"  # Change to "IMMUTABLE" if needed
  image_scanning_configuration {
    scan_on_push = true  # Enable image scanning
  }
}

# IAM policy attachment to allow CodeBuild to push images to the ECR repository
resource "aws_iam_policy_attachment" "ecr_codebuild_attachment" {
  count = var.attach_ecr_policy && var.create_ecr_repository ? 1 : 0  # Attach only if specified and the repository exists
  name       = "ecr_codebuild_attachment"
  roles      = [aws_iam_role.codebuild_role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"  # Adjust policy as needed
}

