#!/bin/bash

exec > >(tee -i /home/ubuntu/script.log)
exec 2>&1


DATABASE_PASS='admin123'

# Update package index
sudo apt-get update

# Install Java
sudo apt-get install openjdk-11-jdk -y

# Verify Java installation
java -version
if [ $? -ne 0 ]; then
    echo "Java installation failed."
    exit 1
fi

# Install Memcached
sudo apt-get install memcached -y
sudo systemctl start memcached
sudo systemctl enable memcached

# Install RabbitMQ
sudo apt-get install socat wget -y
curl -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -
echo "deb https://dl.bintray.com/rabbitmq-erlang/debian bionic erlang-21.x" | sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list
sudo apt-get update
sudo apt-get install rabbitmq-server -y
sudo systemctl start rabbitmq-server
sudo systemctl enable rabbitmq-server
sudo rabbitmqctl add_user rabbit bunny
sudo rabbitmqctl set_user_tags rabbit administrator
sudo systemctl restart rabbitmq-server

# Install MariaDB
sudo apt-get install mariadb-server -y
sudo systemctl start mariadb
sudo systemctl enable mariadb
sudo mysqladmin -u root password "$DATABASE_PASS"
sudo mysql -u root -p"$DATABASE_PASS" -e "CREATE DATABASE accounts"
sudo mysql -u root -p"$DATABASE_PASS" -e "GRANT ALL PRIVILEGES ON accounts.* TO 'admin'@'localhost' IDENTIFIED BY 'admin123'"
sudo mysql -u root -p"$DATABASE_PASS" -e "GRANT ALL PRIVILEGES ON accounts.* TO 'admin'@'%' IDENTIFIED BY 'admin123'"
sudo mysql -u root -p"$DATABASE_PASS" -e "FLUSH PRIVILEGES"

# Install Tomcat
TOMCAT_VERSION="9.0.75"
TOMCAT_URL="https://archive.apache.org/dist/tomcat/tomcat-9/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz"
TOMCAT_DIR="/usr/local/tomcat"
sudo useradd --shell /bin/false tomcat
sudo mkdir -p $TOMCAT_DIR
sudo wget $TOMCAT_URL -O tomcat.tar.gz
sudo tar xzf tomcat.tar.gz --strip-components=1 -C $TOMCAT_DIR
sudo chown -R tomcat.tomcat $TOMCAT_DIR

# Create Tomcat systemd service
sudo tee /etc/systemd/system/tomcat.service <<EOT
[Unit]
Description=Tomcat
After=network.target

[Service]
User=tomcat
Group=tomcat
WorkingDirectory=$TOMCAT_DIR
Environment=JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
Environment=CATALINA_HOME=$TOMCAT_DIR
Environment=CATALINA_BASE=$TOMCAT_DIR
ExecStart=$TOMCAT_DIR/bin/catalina.sh run
ExecStop=$TOMCAT_DIR/bin/shutdown.sh
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
EOT

sudo systemctl daemon-reload
sudo systemctl start tomcat
sudo systemctl enable tomcat

# Deploy application (example)
sudo apt-get install maven -y
git clone -b main https://github.com/devopshydclub/vprofile-project.git
cd vprofile-project
mvn install
sudo systemctl stop tomcat
sudo rm -rf $TOMCAT_DIR/webapps/ROOT*
pwd
sudo cp target/vprofile-v2.war $TOMCAT_DIR/webapps/ROOT.war
sudo systemctl start tomcat

