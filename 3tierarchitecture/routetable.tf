resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "dtcc-public-route-table"
  }
}

resource "aws_route_table_association" "public_rta_az1" {
  subnet_id      = aws_subnet.public_az1.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "public_rta_az2" {
  subnet_id      = aws_subnet.public_az2.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table" "private_rt_az1" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_az1.id
  }

  tags = {
    Name = "dtcc-private-route-table-az1"
  }
}

resource "aws_route_table_association" "private_rta_az1" {
  subnet_id      = aws_subnet.private_az1.id
  route_table_id = aws_route_table.private_rt_az1.id
}

resource "aws_route_table" "private_rt_az2" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_az2.id
  }

  tags = {
    Name = "dtcc-private-route-table-az2"
  }
}

resource "aws_route_table_association" "private_rta_az2" {
  subnet_id      = aws_subnet.private_az2.id
  route_table_id = aws_route_table.private_rt_az2.id
}

# Route tables for the DB servers
resource "aws_route_table" "db_private_rt_az1" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_az1.id
  }

  tags = {
    Name = "dtcc-db-private-route-table-az1"
  }
}

resource "aws_route_table_association" "db_private_rta_az1" {
  subnet_id      = aws_subnet.db_private_az1.id
  route_table_id = aws_route_table.db_private_rt_az1.id
}

resource "aws_route_table" "db_private_rt_az2" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_az2.id
  }

  tags = {
    Name = "dtcc-db-private-route-table-az2"
  }
}

resource "aws_route_table_association" "db_private_rta_az2" {
  subnet_id      = aws_subnet.db_private_az2.id
  route_table_id = aws_route_table.db_private_rt_az2.id
}

