resource "aws_db_subnet_group" "database_subnet_group" {
  name        = "database-subnet-group"
  subnet_ids  = [aws_subnet.db_private_az1.id, aws_subnet.db_private_az2.id]
  description = "Database subnet group for RDS instances"

  tags = {
    Name = "dtcc-database-subnet-group"
  }
}

resource "aws_db_instance" "mydbtestaz1" {
  engine                  = "mysql"
  engine_version          = "8.0.35"
  multi_az                = true
  identifier              = "rds-instanceaz1"
  username                = "nishanth"
  password                = "Dtccnishanthaz1"
  instance_class          = "db.t2.micro"
  allocated_storage       = 20
  db_subnet_group_name    = aws_db_subnet_group.database_subnet_group.name
  vpc_security_group_ids  = [aws_security_group.db_sg.id]
 # availability_zone       = "${var.region}a"
  db_name                 = "dtcctestingaz1"
  skip_final_snapshot     = true
}

resource "aws_db_instance" "mydbtestaz2" {
  engine                  = "mysql"
  engine_version          = "8.0.35"
  multi_az                = true
  identifier              = "rds-instanceaz2"
  username                = "nishanth"
  password                = "Dtccnishanthaz2"
  instance_class          = "db.t2.micro"
  allocated_storage       = 20
  db_subnet_group_name    = aws_db_subnet_group.database_subnet_group.name
  vpc_security_group_ids  = [aws_security_group.db_sg.id]
 # availability_zone       = "${var.region}b"
  db_name                 = "dtcctestingaz2"
  skip_final_snapshot     = true
}

