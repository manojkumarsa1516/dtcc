resource "aws_lb" "alb" {
  name               = "dtcc-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.web_sg.id]
  subnets            = [aws_subnet.public_az1.id, aws_subnet.public_az2.id]

  enable_deletion_protection = false

  tags = {
    Name = "dtcc-alb"
  }
}

resource "aws_lb_target_group" "test_tg" {
  name     = "dtcc-tg"
  port     = 80
  protocol = "HTTP"
  target_type = "instance"
  vpc_id   = aws_vpc.main.id

  health_check {
    enabled  = true
    path     = "/"
    protocol = "HTTP"
    matcher  = "200"
  }

  tags = {
    Name = "dtcc-tg"
  }
}

resource "aws_lb_target_group_attachment" "alb_test_az1" {
  target_group_arn = aws_lb_target_group.test_tg.arn
  target_id        = aws_instance.web_server.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "alb_test_az2" {
  target_group_arn = aws_lb_target_group.test_tg.arn
  target_id        = aws_instance.web_server_az2.id
  port             = 80
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.test_tg.arn
  }
}

