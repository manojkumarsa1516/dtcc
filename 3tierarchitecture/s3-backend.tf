terraform {
  backend "s3" {
    bucket   = "nish-backend"
    key      = "global/mystatefile/terraform.tfstate"
    region   = "ap-south-1"
    encrypt  = true
  }
}

resource "aws_s3_bucket" "nish_backend_bucket" {
  bucket = "nish-backend"

  versioning {
    enabled = true
  }

  tags = {
    Name = "Nish Backend Bucket"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "nish_backend_bucket_sse" {
  bucket = aws_s3_bucket.nish_backend_bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}
